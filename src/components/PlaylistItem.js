import React from "react";
import StylePlayListItem from "./styles/StyledPlaylistItem";

const PlaylistItem = ({ video, active, played }) => (
    <StylePlayListItem active={active} played={played}>
        <div className='wbn-player__video-nr'>{video.num}</div>
        <div className='wbn-player__video-name'>{video.title}</div>
        <div className='wbn-player__video-time'>{video.duration}</div>
    </StylePlayListItem>
);
export default PlaylistItem;
